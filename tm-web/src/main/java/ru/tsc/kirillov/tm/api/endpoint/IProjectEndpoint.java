package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    ProjectDto save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody List<ProjectDto> projects
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();
}