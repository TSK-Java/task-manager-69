package ru.tsc.kirillov.tm.exception.user;

public final class UserPasswordEmptyException extends AbstractUserException {

    public UserPasswordEmptyException() {
        super("Ошибка! Пароль пользователя не задан.");
    }

}
