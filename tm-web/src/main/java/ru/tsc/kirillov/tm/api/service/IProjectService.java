package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDto create(@Nullable String userId);

    @NotNull
    ProjectDto create(@Nullable String userId, @NotNull String name);

    @NotNull
    ProjectDto save(@Nullable String userId, @NotNull ProjectDto project);

    @Nullable
    Collection<ProjectDto> findAll(@Nullable String userId);

    @Nullable
    ProjectDto findById(@Nullable String userId, @NotNull String id);

    void removeById(@Nullable String userId, @NotNull String id);

    void remove(@Nullable String userId, @NotNull ProjectDto project);

    void remove(@Nullable String userId, @NotNull List<ProjectDto> projects);

    boolean existsById(@Nullable String userId, @NotNull String id);

    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
