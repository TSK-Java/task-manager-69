package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.kirillov.tm.dto.model.RoleDto;
import ru.tsc.kirillov.tm.dto.model.UserDto;
import ru.tsc.kirillov.tm.dto.model.UserEx;
import ru.tsc.kirillov.tm.exception.user.UserLoginEmptyException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserDtoRepository userRepository;

    @Nullable
    private UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        return userRepository.findFirstByLogin(login);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String login) throws UsernameNotFoundException {
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException(login);

        @NotNull final User.UserBuilder builder = User.withUsername(login);
        builder.password(user.getPasswordHash());

        @NotNull final List<RoleDto> userRole = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        userRole.forEach(role -> roles.add(role.toString()));
        builder.roles(roles.toArray(new String[] {}));

        @NotNull final UserDetails details = builder.build();
        @NotNull final User userSpring = (User) details;
        @NotNull final UserEx userEx = new UserEx(userSpring, user);

        return userEx;
    }

}
