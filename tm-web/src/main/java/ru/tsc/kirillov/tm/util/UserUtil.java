package ru.tsc.kirillov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.kirillov.tm.dto.model.UserEx;
import ru.tsc.kirillov.tm.exception.AccessDeniedException;

public interface UserUtil {

    static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof UserEx)) throw new AccessDeniedException();
        @NotNull final UserEx userEx = (UserEx) principal;
        return userEx.getUserId();
    }

}
