package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    public ProjectDto create(@Nullable final String userId) {
        @NotNull final ProjectDto project = new ProjectDto(
                userId,
                "Новый проект: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(userId, project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDto create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final ProjectDto project = new ProjectDto(
                userId,
                name,
                "Описание",
                new Date(),
                new Date());
        save(userId, project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDto save(@Nullable final String userId, @NotNull final ProjectDto project) {
        project.setUserId(userId);
        return repository.save(project);
    }

    @Nullable
    @Override
    public Collection<ProjectDto> findAll(@Nullable final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String userId, @NotNull final String id) {
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    public void removeById(@Nullable final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final ProjectDto project) {
        removeById(userId, project.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final List<ProjectDto> projects) {
        projects
                .stream()
                .forEach(project -> remove(userId, project));
    }

    @Override
    public boolean existsById(@Nullable final String userId, @NotNull final String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public void clear(@Nullable final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        return repository.countByUserId(userId);
    }
    
}
