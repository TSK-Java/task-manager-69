package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDto save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        return projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectService.remove(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<ProjectDto> projects) {
        projectService.remove(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id) {
        projectService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count(UserUtil.getUserId());
    }

}
