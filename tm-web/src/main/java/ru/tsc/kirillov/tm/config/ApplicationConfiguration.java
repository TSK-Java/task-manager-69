package ru.tsc.kirillov.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.kirillov.tm.dto.model.AbstractDtoModel;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaAuditing
@EnableTransactionManagement
@ComponentScan("ru.tsc.kirillov.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.tsc.kirillov.tm.api.repository")
public class ApplicationConfiguration {

    @NotNull
    private static final String HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseUrl,
            @NotNull @Value("#{environment['database.username']}") final String databaseUserName,
            @NotNull @Value("#{environment['database.password']}") final String databaseUserPassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUserName);
        dataSource.setPassword(databaseUserPassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("#{environment['database.dialect']}")
            final String databaseDialect,
            @NotNull @Value("#{environment['database.hbm2ddl_auto']}")
            final String databaseHbm2ddlAuto,
            @NotNull @Value("#{environment['database.show_sql']}")
            final String databaseShowSql,
            @NotNull @Value("#{environment['database.format_sql']}")
            final String databaseFormatSql,
            @NotNull @Value("#{environment['database.cache.use_second_level_cache']}")
            final String databaseUseL2Cache,
            @NotNull @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
            final String databaseProviderConfigFileResourcePath,
            @NotNull @Value("#{environment['database.cache.region.factory_class']}")
            final String databaseRegionFactoryClass,
            @NotNull @Value("#{environment['database.cache.user_query_cache']}")
            final String databaseUserQueryCache,
            @NotNull @Value("#{environment['database.cache.use_minimal_puts']}")
            final String databaseUseMinimalPuts,
            @NotNull @Value("#{environment['database.cache.hazelcast.use_lite_member']}")
            final String databaseUseLiteMember,
            @NotNull @Value("#{environment['database.cache.region_prefix']}")
            final String databaseRegionPrefix
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractDtoModel.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddlAuto);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.FORMAT_SQL, databaseFormatSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseUseL2Cache);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProviderConfigFileResourcePath);
        properties.put(Environment.CACHE_REGION_FACTORY, databaseRegionFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, databaseUserQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, databaseUseMinimalPuts);
        properties.put(HAZELCAST_USE_LITE_MEMBER, databaseUseLiteMember);
        properties.put(Environment.CACHE_REGION_PREFIX, databaseRegionPrefix);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}