package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Override
    public TaskDto create(@Nullable final String userId) {
        @NotNull final TaskDto task = new TaskDto(
                userId,
                "Новая задача: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(userId, task);
        return task;
    }

    @NotNull
    @Override
    public TaskDto create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final TaskDto task = new TaskDto(
                userId,
                name,
                "Описание",
                new Date(),
                new Date());
        save(userId, task);
        return task;
    }

    @NotNull
    @Override
    public TaskDto save(@Nullable final String userId, @NotNull final TaskDto task) {
        task.setUserId(userId);
        return repository.save(task);
    }

    @Nullable
    @Override
    public Collection<TaskDto> findAll(@Nullable final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String userId, @NotNull final String id) {
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    public void removeById(@Nullable final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final TaskDto task) {
        removeById(userId, task.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final List<TaskDto> tasks) {
        tasks
                .stream()
                .forEach(task -> remove(userId, task));
    }

    @Override
    public boolean existsById(@Nullable final String userId, @NotNull final String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public void clear(@Nullable final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        return repository.countByUserId(userId);
    }
    
}
