<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
</body>
    <head>
        <title>Task Manager</title>
    </head>
    <style>
        h1 {
            font-size: 1.6em;
        }

        a {
            color: darkblue;
        }

        select {
            width: 200px;
        }

        input[type="text"] {
            width: 200px;
        }

        input[type="date"] {
            width: 200px;
        }
    </style>

<body>
    <table width="100%" height="100%" border="1" style="padding: 10px;">
        <tr>
            <td height="35" width="200" nowrap="nowrap" align="center">
                <b>Task Manager</b>
            </td>

            <td width="400" nowrap="nowrap" align="right">
                <sec:authorize access="isAuthenticated()">
                    <a href="/projects">Проекты</a>
                        |
                    <a href="/tasks">Задачи</a>
                        |
                    Пользователь: <sec:authentication property="name"/>
                        |
                    <a href="/logout">Выйти</a>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                    <a href="/login">Авторизация</a>
                </sec:authorize>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="100%" valign="top" style="padding: 10px;">