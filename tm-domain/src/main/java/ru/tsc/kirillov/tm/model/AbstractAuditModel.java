package ru.tsc.kirillov.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditModel extends AbstractModel {

    @NotNull
    protected static final String FORMAT_DATE_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_ISO_8601)
    private Date createdDate;

    @LastModifiedDate
    @Column(name = "modified_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_ISO_8601)
    private Date modifiedDate;

}
