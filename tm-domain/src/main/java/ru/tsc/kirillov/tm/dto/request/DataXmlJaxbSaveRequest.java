package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlJaxbSaveRequest extends AbstractUserRequest {

    public DataXmlJaxbSaveRequest(@Nullable final String token) {
        super(token);
    }

}
