package ru.tsc.kirillov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.listener.EntityListener;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners({EntityListener.class, AuditingEntityListener.class})
public class UserDto extends AbstractDtoModel {

    private static final long serialVersionUID = 2;

    @Nullable
    @Column(nullable = false, unique = true)
    private String login;

    @Nullable
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(nullable = false, unique = true)
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

    public UserDto(@Nullable final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDto(@Nullable final String login, @Nullable final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDto(@Nullable final String login, @Nullable final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

}
