package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

@NoArgsConstructor
public class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable final ProjectDto project) {
        super(project);
    }

}
