package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListCommandListener {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение доступных команд.";
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #event.name || @applicationHelpListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd);
    }

}
