package ru.tsc.kirillov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractProjectTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
