package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить проект по его ID.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Удаление проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull ProjectRemoveByIdResponse response = getProjectTaskEndpoint().removeProjectById(request);
        @Nullable final ProjectDto project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

}
