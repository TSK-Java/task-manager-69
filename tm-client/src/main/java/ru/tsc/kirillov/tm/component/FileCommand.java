package ru.tsc.kirillov.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.enumerated.FileEventKind;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileCommand {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Nullable
    private FileWatcher watcher;

    @NotNull
    final File folder = new File("./Command");

    private void init() {
        prepareFolder(folder);
        watcher = new FileWatcher(folder);
        watcher.addListener(event -> {
            if (event.getKind() != FileEventKind.CREATE) return;
            @NotNull File file = event.getFile();
            if (file.isDirectory()) return;
            @NotNull String fileName = file.getName();
            int attempt = 0;
            while(!file.delete() && attempt++ < 10);
            publisher.publishEvent(new ConsoleEvent(fileName));
        });
        watcher.startWatch();
    }

    @SneakyThrows
    private void prepareFolder(@NotNull final File folder) {
        if (!folder.exists()) {
            folder.mkdir();
            return;
        }
        @NotNull DirectoryStream.Filter<Path> filter = entry -> !Files.isDirectory(entry);
        try (@NotNull final DirectoryStream<Path> directoryStream =
                     Files.newDirectoryStream(folder.toPath(), filter)) {
            for (@NotNull final Path entry : directoryStream) {
                Files.delete(entry);
            }
        }
    }

    public void start() {
        if (watcher == null)
            init();
    }

    public void stop() {
        if (watcher != null)
            watcher.stopWatch();
    }

}
