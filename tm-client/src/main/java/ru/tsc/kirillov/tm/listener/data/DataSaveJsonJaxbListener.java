package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataJsonJaxbSaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataSaveJsonJaxbListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-save-json-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из json файла (JAXB API)";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataSaveJsonJaxbListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Сохранение состояния приложения из json файла (JAXB API)]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonJaxbSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
