package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.model.IUserRepository;
import ru.tsc.kirillov.tm.api.repository.model.IWbsRepository;
import ru.tsc.kirillov.tm.api.service.model.IWbsService;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.AbstractWbsModel_;
import ru.tsc.kirillov.tm.model.User;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractWbsService<M extends AbstractWbsModel, R extends IWbsRepository<M>>
        extends AbstractUserOwnedService<M, R>
        implements IWbsService<M> {
    
    @NotNull
    @Override
    protected abstract IWbsRepository<M> getRepository();

    @NotNull
    @Autowired
    protected IUserRepository userRepository;

    @NotNull
    protected org.springframework.data.domain.Sort getSort(@NotNull final Comparator comparator) {
        @NotNull final String fieldName;
        if (comparator == CreatedComparator.INSTANCE) fieldName = AbstractWbsModel_.CREATED_DATE;
        else if (comparator == StatusComparator.INSTANCE) fieldName = AbstractWbsModel_.STATUS;
        else if (comparator == DateBeginComparator.INSTANCE) fieldName = AbstractWbsModel_.DATE_BEGIN;
        else fieldName = AbstractWbsModel_.NAME;
        return Sort.by(Sort.Direction.ASC, fieldName);
    }

    @Nullable
    @Override
    public M create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @NotNull final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user.get());
        return repository.save(model);
    }

    @Nullable
    @Override
    public M create(@Nullable final User user, @NotNull final String name) {
        if (user == null) throw new UserNotFoundException();
        return create(user.getId(), name);
    }

    @Nullable
    @Override
    public M create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @NotNull final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user.get());
        model.setDescription(description);
        return repository.save(model);
    }

    @Nullable
    @Override
    public M create(@Nullable final User user, @NotNull final String name, @NotNull final String description) {
        if (user == null) throw new UserNotFoundException();
        return create(user.getId(), name, description);
    }

    @Nullable
    @Override
    public M create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @NotNull final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @NotNull final M model = newInstance();
        model.setName(name);
        model.setUser(user.get());
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return repository.save(model);
    }

    @Nullable
    @Override
    public M create(
            @Nullable final User user,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd) {
        if (user == null) throw new UserNotFoundException();
        return create(user.getId(), name, description, dateBegin, dateEnd);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final IWbsRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId, getSort(comparator));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final ru.tsc.kirillov.tm.enumerated.Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final ru.tsc.kirillov.tm.enumerated.Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        @NotNull final IWbsRepository<M> repository = getRepository();
        return repository.findAll(getSort(comparator));
    }

    @Nullable
    @Override
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @Nullable final M model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public M updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public M changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final IWbsRepository<M> repository = getRepository();
        @Nullable final M model;
        model = findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }
    
}
