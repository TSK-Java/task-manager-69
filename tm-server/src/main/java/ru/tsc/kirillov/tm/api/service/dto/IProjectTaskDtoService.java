package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

public interface IProjectTaskDtoService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    ProjectDto removeProjectById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    ProjectDto removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

}
