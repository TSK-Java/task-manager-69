package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findFirstByLogin(@NotNull String login);

    @Nullable
    User findFirstByEmail(@NotNull String email);

    @Nullable
    @Transactional
    User deleteByLogin(@NotNull String login);

}
